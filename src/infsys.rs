pub trait Aggregabble<Where, What, Final> {
    type State;

    fn new() -> Self::State;
    fn append(&mut self, _: &Where, _: &What);
    fn merge(&mut self, with: Self::State);
    fn reduce(self) -> Final;
}

pub trait InformationSystem {
    type Feature: Copy + Sized;
    type Subset;

    type Pivot: Sized + Copy;
    type Leaf: Sized + Copy;

    type Decision;
    type Vote: Aggregabble<Self::Subset, Self::Leaf, Self::Decision>;

    fn bag(&self) -> (Self::Subset, Self::Subset);
    fn split(&self, what: &Self::Subset, with: &Self::Pivot) -> (Self::Subset, Self::Subset);
    fn feature(of: Self::Pivot) -> Self::Feature;
    fn pivot(&self, what: &Self::Subset) -> (Self::Pivot, Self::Subset, Self::Subset);
    fn subset_of_all(&self) -> Self::Subset;
    fn try_leaf(&self, _: &Self::Subset) -> Option<Self::Leaf>;
    fn force_leaf(&self, _: &Self::Subset) -> Self::Leaf;
    fn decision(&self, _: &Self::Subset) -> Self::Decision;
    fn accuracy(a: &Self::Decision, b: &Self::Decision) -> f64;
}
