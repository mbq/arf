use crate::infsys::Aggregabble;
use crate::infsys::InformationSystem; //TODO: spell this right

pub struct ArfBuilder<'a, IS>
where
    IS: InformationSystem,
{
    infsys: &'a IS,
    ntree: usize,
    max_depth: usize,
    oob_vote: Option<IS::Vote>,
}

pub enum Tree<IS: InformationSystem> {
    Branch(IS::Pivot, Box<Tree<IS>>, Box<Tree<IS>>),
    Leaf(IS::Leaf),
}

impl<IS: InformationSystem> Tree<IS> {
    fn new(infsys: &IS, on: &IS::Subset, depth_left: usize) -> Tree<IS> {
        let leaf: Option<IS::Leaf> = if depth_left == 0 {
            Some(infsys.force_leaf(on))
        } else {
            infsys.try_leaf(on)
        };
        if let Some(leaf) = leaf {
            Tree::Leaf(leaf)
        } else {
            let (pivot, left, right) = infsys.pivot(on);
            Tree::Branch(
                pivot,
                Box::new(Self::new(infsys, &left, depth_left - 1)),
                Box::new(Self::new(infsys, &right, depth_left - 1)),
            )
        }
    }
    fn predict(&self, infsys: &IS, on: &IS::Subset, vote: &mut IS::Vote) {
        match self {
            Self::Leaf(leaf) => vote.append(on, leaf),
            Self::Branch(pivot, left_tree, right_tree) => {
                let (left, right) = infsys.split(on, pivot);
                left_tree.predict(infsys, &left, vote);
                right_tree.predict(infsys, &right, vote);
            }
        }
    }
}

pub struct Arf<IS: InformationSystem> {
    //infsys:&'a IS,
    forest: Option<Vec<Tree<IS>>>,
}

impl<'a, IS> ArfBuilder<'a, IS>
where
    IS: InformationSystem,
{
    fn tree(&mut self) -> (Tree<IS>, IS::Subset) {
        let (bag, oob) = self.infsys.bag();
        let tree = Tree::new(self.infsys, &bag, self.max_depth);
        self.oob_vote
            .iter_mut()
            .map(|mut oob_vote| {
                tree.predict(self.infsys, &oob, oob_vote);
            })
            .next();
        (tree, bag)
    }
    // fn tree_predict(&self,
}
